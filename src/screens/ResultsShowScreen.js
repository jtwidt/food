import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  Image,
  ScrollView,
} from 'react-native';
import yelp from '../api/yelp';

const ResultsShowScreen = ({ navigation }) => {
  const [result, setResult] = useState(null);
  const id = navigation.getParam('id');

  const getResult = async (id) => {
    const response = await yelp.get(`/${id}`);
    setResult(response.data);
  };

  useEffect(() => {
    getResult(id);
  }, []);

  if (!result) {
    return null;
  }

  return (
    <View style={styles.container}>
      <View style={styles.addressContainer}>
        <Text style={styles.nameStyle}>{result.name}</Text>
        <Text>
          {result.location.address1} {result.location.address2}{' '}
          {result.location.address3}
        </Text>
        <Text>
          {result.location.city}, {result.location.state}{' '}
          {result.location.zip_code}
        </Text>
        <Text>{result.display_phone}</Text>
      </View>
      <FlatList
        data={result.photos}
        keyExtractor={(photo) => photo}
        showsVerticalScrollIndicator={false}
        renderItem={({ item }) => {
          return <Image source={{ uri: item }} style={styles.imageStyle} />;
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
  },
  addressContainer: {
    alignItems: 'center',
    marginTop: 10,
  },
  imageStyle: {
    height: 200,
    width: 300,
    marginVertical: 10,
  },
  nameStyle: {
    fontWeight: 'bold',
  },
});

export default ResultsShowScreen;
